http_path = "/"
css_dir = "_public"
sass_dir = "_assets/styles"
images_dir = "_assets/images"
javascripts_dir = "_assets/js"
fonts_dir = "_assets/fonts"

output_style = :compressed
relative_assets = true
line_comments = false
color_output = false
preferred_syntax = :scss
